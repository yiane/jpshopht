import request from '@/utils/request'

//问题分组

/**
 * 获取问题分组列表
 * @param {*} params 
 */
export function getGroupList(params) {
  return request({
    url: '/merchantAppHelpCategory',
    method: 'get',
    params
  })
}
/**
 * 添加问题分组
 * @param {*} params 
 */
export function postGroup(params) {
  return request({
    url: '/merchantAppHelpCategory',
    method: 'post',
    data: params
  })
}
/**
 * 修改问题分组
 * @param {*} params 
 */
export function putGroup(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantAppHelpCategory/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除问题分组
 * @param {*} params 
 */
export function delGroup(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantAppHelpCategory/'+id,
    method: 'delete',
    data: params
  })
}


//问题列表

/**
 * 获取问题列表
 * @param {*} params 
 */
export function getHelpList(params) {
  return request({
    url: '/merchantAppHelp',
    method: 'get',
    params
  })
}
/**
 * 新增问题
 * @param {*} params 
 */
export function postHelp(params) {
  return request({
    url: '/merchantAppHelp',
    method: 'post',
    data: params
  })
}
/**
 * 修改问题
 * @param {*} params 
 */
export function putHelp(params) {
  const id = params.id;
  delete params.id
  return request({
    url: '/merchantAppHelp/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除问题
 * @param {*} params 
 */
export function delHelp(params) {
  const id = params.id;
  delete params.id
  return request({
    url: '/merchantAppHelp/'+id,
    method: 'delete',
    data: params
  })
}