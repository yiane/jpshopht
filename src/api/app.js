import request from '@/utils/request'

/**
 * 获取应用列表
 */
export function merchantApp() {
  return request({
    url: '/merchantApp',
    method: 'get'
  })
}

/**
 * 获取店铺类型列表
 */
export function merchantSHopCategor() {
  return request({
    url: '/merchantShopCategory',
    method: 'get'
  })
}

/**
 * 创建应用
 */
export function merchantPay(params) {
  return request({
    url: '/merchantPay',
    method: 'post',
    params
  })
}